#Installation

Clone this repository

```bash
git clone git@bitbucket.org:iracooke/functional-annotator.git
```

Execute all commands from within the `Functional annotator` directory of this repository

##Prerequisites

- Python2.7 
- Biopython
- gdbm or other dbm implementation that supports very large databases

##Download datasets


```bash
wget ftp://ftp.ncbi.nlm.nih.gov/gene//DATA/gene2go.gz

wget ftp://ftp.ncbi.nlm.nih.gov/gene//DATA/gene2accession.gz
```


## Unzip databases
```bash
gunzip gene2go.gz

gunzip gene2accession.gz
```


##Make Database files
*MakeProtein2GeneDB will take ~30 minutes

```bash
python MakeSequenceDB.py yourfastafile.fasta

python MakeProtein2GeneDB.py

python MakeGene2GoDB.py
```



#Running annotation.py 

```bash
python annotation.py yourblastoutput.xml
```




#Understanding output

Annotation.py will annotate your sequence into two files. "best_hits.py" will annotate your sequences with information from their top hit from your blast search.
"best_go_hits.txt" will annotate your sequences with information from their best blast hit that posssessed a GO term from your blast search.

File will be tab delimited:
```bash
e.g. description	species    E value	  NCBI_GI_number	gene_ID    GO terms	   sequence    hit number(1= top hit, 2 = second hit etc)
```

