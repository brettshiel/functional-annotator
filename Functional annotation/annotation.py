
from Bio import Blast
from Bio.Blast import NCBIXML, NCBIWWW
import anydbm
import sys

from Bio.Seq import Seq
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from Bio.Alphabet import IUPAC


#input_filename = str(raw_input('enter filename: ')) 
input_filename= sys.argv[1]
gene_database = anydbm.open('./Protein2Gene.dat', 'r')
go_database = anydbm.open('./GeneID2GoDB.dat', 'r')
blastoutput = open(input_filename, 'r')
trinity = anydbm.open('./Sequence.dat', 'r')
parser = Blast.NCBIXML.parse(blastoutput)
best_go_hits = open('best_go_hits.txt', 'w')
best_e_hits = open('best_hits.txt', 'w')



#function runs through e values of a sequence matches and returns the hit with the lowest e value
def best_hit_index(hits):

	minimum = .00001
	minIndex = None
	
	for i in range((len(hits))):
		if hits[i].e < minimum:

			minimum = hits[i].e
			minIndex = i
			
	return minIndex



def best_hit_index_with_go(hits):
	
	minimum = .00001
	minIndex = None
	
	for i in range((len(hits))):
		if 	hits[i].e < minimum:
			if hits[i].e < minimum and get_go_terms(get_hit_gi(i)) != str('no go terms assigned'):
				minimum = hits[i].e
				minIndex = i

			
	return minIndex

def get_geneid(hit_gi):
	geneid = "not available "
	if str(hit_gi) in gene_database:
		geneid = gene_database[hit_gi]

		return geneid


def get_go_terms(hit_gi):
	goterms = "no go terms assigned"

	
	if str(hit_gi) in gene_database:
		geneid = gene_database[hit_gi]
		if geneid in go_database:
			goterms = go_database[geneid]


	return goterms



def get_title(hit):
	title = "not available"
	title = entry.alignments[hit].hit_def.split('[')[0]
	
	return title



def get_species(hit):
	species = "not available"
	try:

		species = entry.alignments[hit].hit_def.split('[')[1]
		species = species.split(']')[0]
		
	
		return species
	except:
		pass



def get_hit_gi(hit):
	gi= "not available"
	gi = entry.alignments[hit].hit_id.split('|')[1]

	return gi
		

def get_e(hit):
	e = sorted_hits[hit].e
	
	return e


def get_sequence():
	queryID = entry.query.split(' ')[0]
	query_sequence = trinity[queryID]

	return query_sequence
			



for entry in parser:

	sorted_hits = sorted(entry.descriptions,key=lambda hit: hit.e)


	go_hit_i = best_hit_index_with_go(sorted_hits)
	if  go_hit_i != None :
		go_hit_gi = get_hit_gi(go_hit_i)
		
		HITS_WITH_GO = '%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s' % (get_title(go_hit_i),get_species(go_hit_i),get_e(go_hit_i),go_hit_gi,get_geneid(go_hit_gi),get_go_terms(go_hit_gi),get_sequence(),go_hit_i)
		print HITS_WITH_GO
		HITS_WITH_GO_data = ''
		for line in HITS_WITH_GO.split('\n'):

			HITS_WITH_GO_data += line + '\n'

		
		best_go_hits.write(HITS_WITH_GO_data)

	


	besthit_i = best_hit_index(sorted_hits)
	if  besthit_i != None :
		hit_gi = get_hit_gi(besthit_i)


		
		
		BEST_HITS = '%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s' % (get_title(besthit_i),get_species(besthit_i),get_e(besthit_i),hit_gi,get_geneid(hit_gi),get_go_terms(hit_gi),get_sequence(),besthit_i)


		BEST_HITS_data = ''
		for line in BEST_HITS.split('\n'):

			BEST_HITS_data += line + '\n'

			# print BEST_HITS_data


		best_e_hits.write(BEST_HITS_data)


# STATS THAT CAN BE USED LATEER TO FIGURE OUT ARTIFICIAL SEQUENCER PAIRING etc...

		# bit_score = entry.alignments[0].hsps[0].bits
						
		# hit_length = entry.alignments[0].length
						
		# align_length = entry.alignments[0].hsps[0].align_length

		# percent = (float(align_length)/ float(hit_length) * 100)


		# a = sorted_hits[0].num_alignments

		# query_start = entry.alignments[0].hsps[0].query_start
		# query_end = entry.alignments[0].hsps[0].query_end
		# sequence_start = entry.alignments[0].hsps[0].sbjct_start
		# sequence_end= entry.alignments[0].hsps[0].sbjct_end


		
						
		# #determines percentage cover of the fragment to the aligned sequence
		# if percent >= 90:
		# 	score = 'good score'
		# if percent < 90 and percent >70: 
		# 	score =  'ok score'
		# if percent < 70 and percent > 50:
		# 	score =  'not great'
		# if percent <= 50:
		# 	score = 'bad score'
						

		
						

		










			# print HITS_WITH_GO_data























	
			

		