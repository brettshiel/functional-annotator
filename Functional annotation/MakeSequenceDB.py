from Bio import SeqIO
import anydbm
import sys

id2seq =anydbm.open('Sequence.dat', 'n')

#fasta_fileName = str(raw_input('enter fasta file e.g. "assemley.fast": ')) 
fasta_fileName = sys.argv[1]
new_fasta_file = open(fasta_fileName, 'r')

for record in SeqIO.parse(new_fasta_file, "fasta") :
	id2seq[record.id]=str(record.seq)
